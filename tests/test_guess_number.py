import guessnumber.guess_number as guess_number
import unittest
from unittest import mock
import random
class TestGuessNumber(unittest.TestCase):
    @mock.patch('random.randint')
    @mock.patch('random.uniform')
    def test_guess_random_use_random_randint_and_random_uniform_all_called(self,random_randint,random_uniform):
        start =mock.MagicMock()
        stop = mock.MagicMock()
        guess_number.guess_int(start,stop)
        guess_number.guess_float(start,stop)
        random_uniform.assert_called()
        random_randint.assert_called()

    @mock.patch('random.randint')
    def test_guess_int_range1_10_should_be_celles_random_randint(self,random_randint):
        guess_number.guess_int(1,10)
        random_randint.assert_called_once_with(1, 10)
        random_randint.assert_called()

    @mock.patch('random.uniform')
    def test_guess_random_uniform_range_1_10_should_be_celles_random_uniform(self,random_uniform):
        guess_number.guess_float(1,10)
        random_uniform.assert_called_once_with(1, 10)
        random_uniform.assert_called()
    
    @mock.patch('random.randint')
    @mock.patch('random.uniform')
    def test_random_randint_range1_100_should_be_return_int_range1_100(self,random_randint,random_uniform):
        random_number =guess_number.guess_int(1,100)
        random_randint.return_value = random_number
        self.assertEqual(random_randint(1,100),random_number)

    @mock.patch('random.randint')
    @mock.patch('random.uniform')
    def test_random_uniform_range1_100_should_be_return_float_range1_100(self,random_randint,random_uniform):
        random_number =guess_number.guess_float(1,100)
        random_uniform.return_value = random_number
        self.assertEqual(random_uniform(1,100),random_number)