'''
    This module is random number generator.
'''

import random


def guess_int(start, stop):
    '''
        Function will return int number
    '''
    return random.randint(start, stop)


def guess_float(start, stop):
    '''
        Function will return float number
    '''
    return random.uniform(start, stop)
